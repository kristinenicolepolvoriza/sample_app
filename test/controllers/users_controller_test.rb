require 'test_helper'

class UsersControllerTest < ActionController::TestCase

	def setup
		@user = users(:michael)
		@other_user = users(:archer)
	end
  
  test "should get new" do
    get :new
    assert_response :success
  end

  def setup
  	@user = users(:michael)
  	@other_user = users(:archer)
  end

  test "should redirect index when not logged in" do
  	get :index
  	assert_redirected_to login_url
  end

  test "should redirect edit when not logged in as wrong user" do
  	get :edit, id: @user
  	assert_not flash.empty?
  	assert_redirected_to login_url
  end

  test "should redirect update when not logged in as wrong user" do
  	patch :update, id: @user, user: {name: @user.name,
  									email: @user.email }
  	assert_not flash.empty?
  	assert_redirected_to login_url
  end

   test "should not allow the admin attribute to be edited via the web" do
    log_in_as(@other_user)
    assert_not @other_user.admin?
    patch :index, id: @other_user,  user: { password:              "user's password",
                                            password_confirmation: "user's password",
                                            admin: true} 
    assert_not @other_user.reload.admin?
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'User.count' do
      delete :destroy ,id: @user
    end
    assert_redirected_to login_url
  end

 test "should redirect destroy when logged in as a non-admin" do
    log_in_as(@other_user)
    assert_no_difference 'User.count' do
      delete :destroy, id: @user
    end
    assert_redirected_to root_url 
  end
end
